<?php

namespace App\Http\Middleware;

use App\Post;
use Closure;

class ValidateAuthorForEditAndDeletePost
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(is_object($request->post))
        {
            if(!($request->post->author->id == auth()->id()))
            {
                return redirect(abort(401));
            }
        }
        elseif(is_numeric($request->post))
        {
            $post = Post::onlyTrashed()->findOrFail($request->post);
            if(!($post->user_id == auth()->id()))
            {
                return redirect(abort(401));
            }
        }
        return $next($request);
    }
}
