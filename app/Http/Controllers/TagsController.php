<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTagRequest;
use App\Http\Requests\UpdateTagRequest;
use App\Tag;
use Illuminate\Http\Request;

class TagsController extends Controller
{
    public function index()
    {
        $tags = Tag::all();
        return view('tags.index', compact(['tags']));
    }
    public function create()
    {
        return view('tags.create');
    }
    public function store(CreateTagRequest $request)
    {
        Tag::create([
            'name'=>$request->name
        ]);


        // 3.Session set something and then return a view
        session()->flash('success', 'Tag Added Successfully');

        return redirect(route('tags.index'));
    }

    public function show(Tag $tag)
    {
        //
    }

    public function edit(Tag $tag)
    {
        return view('tags.edit', compact(['tag']));
    }
    public function update(UpdateTagRequest $request, Tag $tag)
    {
        $tag->name = $request->name;
        $tag->save();
        // $tag->update(['name'=> $request->name]);
        session()->flash('success', 'Tag Updated Successfully');
        return redirect(route('tags.index'));
    }
    public function destroy(Tag $tag)
    {
        if($tag->posts->count() > 0)
        {
            session()->flash('error', 'Tag cannot be deleted as some posts are registered to it!');
            return redirect()->back();
        }
        $tag->delete();
        session()->flash('success', 'Tag Deleted Successfully');
        return redirect(route('tags.index'));
    }
}
