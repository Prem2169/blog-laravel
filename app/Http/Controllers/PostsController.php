<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\posts\CreatePostRequest;
use App\Http\Requests\posts\UpdatePostRequest;
use App\Post;
use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class PostsController extends Controller
{
    public function __construct()
    {
        $this->middleware('verifyCategoriesCount')->only('create', 'store');
        $this->middleware('validateAuthor')->only('edit', 'update', 'destroy', 'trash');
    }
    public function index()
    {
        if(!auth()->user()->isAdmin())
        {
            $posts = Post::withoutTrashed()->where('user_id' , auth()->id())->paginate(10);
        }
        else{

            $posts = Post::paginate(10);
        }
        return view('posts.index', compact([
            'posts'
        ]));
    }
    public function create()
    {
        $categories = Category::all();
        $tags = Tag::all();
        return view('posts.create', compact([
            'categories' ,
            'tags'
        ]));
    }
    public function store(CreatePostRequest $request)
    {
        //Image upload  and stores the name of the image

        // dd($request->tags);

        $image = $request->file('image')->store('posts');

        //Image upload  and stores the name of the image
        $post = Post::create([
            'title' => $request->title,
            'excerpt' => $request->excerpt,
            'content' => $request->content,
            'image' => $image,
            'published_at' => $request->published_at,
            'user_id' => auth()->id(),
            'category_id' => $request->category
        ]);

        $post->tags()->attach($request->tags);

        //Session storage
        session()->flash('success', 'Post Created Successfully');

        //redirect
        return redirect(route('posts.index'));

        /**
         * These are the 4 steps of creation
         *  i.e
         *
         * Image upload  and stores the name of the image
         * Create post
         * Session storage
         * redirect
         */
    }
    public function show(Post $post)
    {
        //
    }
    public function edit(Post $post)
    {
        $categories = Category::all();
        $tags = Tag::all();
        return view('posts.edit', compact([
            'post',
            'categories',
            'tags'
        ]));
    }
    public function update(UpdatePostRequest $request, Post $post)
    {
        $data = $request->only(['title', 'excerpt', 'content', 'published_at', 'category_id']);
        if($request->hasFile('image'))
        {
            //Here my deleteImage() was not working
            // $post->deleteImage();
            File::delete(public_path('storage/'.$post->image));
            $image = $request->image->store('posts');
            $data['image'] = $image;
        }
        $post->update($data);
        if($request->tags)
        {
            $post->tags()->sync($request->tags);
        }
        session()->flash('success', 'Post Updated Successfully');
        return redirect(route('posts.index'));

    }
    public function destroy($id)
    {
        // dd($id);
        $post = Post::onlyTrashed()->findOrFail($id);
        $post->forceDelete();
        session()->flash('success', 'Post Permenantely Deleted Successfully');
        return redirect(route('posts.trashed'));
    }

    /**
     * Soft Deletes
     */
    public function trash(Post $post)
    {
        // dd("HEllo");
        $post->delete();
        session()->flash('success', 'Post Deleted Successfully');
        return redirect(route('posts.index'));
    }
    public function trashed()
    {
        $posts = Post::onlyTrashed()->paginate(10);
        // $trashed = Post::withTrashed(); //name se he difference pechan lo
        return view('posts.trashed', compact([
            'posts'
        ]));
    }
    public function restore($post_id)
    {
        $post = Post::onlyTrashed()->find($post_id);
        $post->deleted_at = null;
        $post->save();
        session()->flash('success', 'Post Restored Successfully');
        return redirect(route('posts.trashed'));
    }
}
