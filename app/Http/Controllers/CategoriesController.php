<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\categories\CreateCategoryRequest;
use App\Http\Requests\categories\UpdateCategoryRequest;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        return view('categories.index', compact([
            'categories'
        ]));
    }
    public function create()
    {
        return view('categories.create');
    }
    public function store(CreateCategoryRequest $request)
    {
        //1. Validate Data
        //This validation is done from the 'CreateCategoryRequest' Class

        // 2.Store inside the Database
        /**
         * mass assign we have to define in the Model class that which fileds have to be added by using the 'protected fillable' property or using 'protected guarded ' property
         * i.e fillable array ke andar 'name' dalke rakho ya jo bhi dalna ho db m nhi toh mass assign ka error fekega
         * bcoz 'create()' fillabe ya guarded use krta hoga
         */

        // Category::create($request->all());
        Category::create([
            'name'=>$request->name
        ]);


        // 3.Session set something and then return a view
        session()->flash('success', 'Category Added Successfully');

        return redirect(route('categories.index'));
    }
    public function show(Category $category)
    {
        //
    }
    public function edit(Category $category)
    {
        // dd($category);
        return view('categories.edit', compact([
            'category'
        ]));
    }
    public function update(UpdateCategoryRequest $request, Category $category)
    {
        $category->name = $request->name;
        $category->save();
        // $category->update(['name'=> $request->name]);
        session()->flash('success', 'Category Updated Successfully');
        return redirect(route('categories.index'));
    }
    public function destroy(Category $category)
    {
        if($category->posts->count() > 0)
        {
            session()->flash('error', 'Category cannot be deleted as some posts are registered to it!');
            return redirect()->back();
        }
        $category->delete();
        session()->flash('success', 'Category Deleted Successfully');
        return redirect(route('categories.index'));
    }
}
