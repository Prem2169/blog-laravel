<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use App\Tag;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    public function index()
    {
        // BEFORE USING QUERY SCOPE
        // $search = request('search');
        // if($search)
        // {
        //     $posts = Post::where('title', 'like', "%$search%")->paginate(2);
        // }
        // else
        // {
        //     $posts = Post::paginate(2);
        // }
        // $categories = Category::all();
        // $tags = Tag::all();
        // return view('blog.index', compact([
        //     'posts',
        //     'tags',
        //     'categories'
        // ]));

        //AFTER USING QUERY SCOPE
        $categories = Category::all();
        $tags = Tag::all();
        $posts = Post::search()->published()->paginate(2);
        return view('blog.index', compact([
            'posts',
            'tags',
            'categories'
        ]));
    }

    public function show(Post $post)
    {
        // dd($post);
        $categories = Category::all();
        $tags = Tag::all();
        return view('blog.post', compact([
            'post',
            'tags',
            'categories'
        ]));
    }

    public function category(Category $category)
    {
        // dd($category);
        $categories = Category::all();
        $tags = Tag::all();

        // Here below used the query scoping i.e used 'search's
        $posts = $category->posts()->search()->published()->paginate(2);
        return view('blog.index', compact([
            'posts',
            'tags',
            'categories'
        ]));
    }

    public function tag(Tag $tag)
    {
        $categories = Category::all();
        $tags = Tag::all();

        // Here below used the query scoping i.e used 'search's
        $posts = $tag->posts()->search()->published()->paginate(2);
        return view('blog.index', compact([
            'posts',
            'tags',
            'categories'
        ]));
    }
}
