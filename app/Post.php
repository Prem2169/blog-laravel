<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;
    protected $dates = ['published_at'];
    protected $fillable = ['title', 'excerpt', 'content', 'image', 'published_at', 'category_id', 'tags', 'user_id'];

    /**
     * RELATIONSHIP METHODS
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    public function tags()
    {
        return $this->belongsToMany(Tag::class)->withTimestamps();
    }
    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * QUERY SCOPE
     */
    public function scopePublished($query)
    {
        return $query->where('published_at', '<=', now());
    }
    public function scopeSearch($query)
    {
        $search = request('search');
        // dd($search);
        if($search)
        {
            return $query->where('title', 'like', "%$search%");
        }
        return $query;
    }
}
