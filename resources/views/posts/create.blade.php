@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">Add Post</div>
        <div class="card-body">
            <form action="{{ route('posts.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text"
                            value="{{ old('title') }}"
                            class="form-control @error('title') is-invalid @enderror"
                            name="title" id="title">
                    @error('title')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="excerpt">Excerpt</label>
                    <textarea name="excerpt" id="excerpt" rows="4" class="form-control @error('excerpt') is-invalid @enderror" >{{ old('excerpt') }}</textarea>
                    @error('excerpt')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="content">Content</label>
                    <input type="hidden" id="content" name="content" value="{{ old('content') }}">
                    <trix-editor input="content"></trix-editor>
                    @error('content')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="category">Category</label>
                    <select name="category" id="category" class="form-control">
                        <option value="-1" disabled selected>Select ... </option>
                        @foreach ($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                    @error('category')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="tag">Tags</label>
                    <select name="tags[]" id="tag" class="form-control select2" multiple>
                        @foreach ($tags as $tag)
                            <option
                                value="{{ $tag->id }}"
                                {{
                                    old('tags') ?
                                    (in_array($tag->id, old('tags')) ? 'selected' : '') : ''
                                }}>
                                {{ $tag->name }}
                            </option>
                        @endforeach
                    </select>
                    @error('tag')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="published_at">Published At</label>
                    <input type="date"
                            value="{{ old('published_at') }}"
                            class="form-control"
                            name="published_at" id="published_at">
                </div>
                <div class="form-group">
                    <label for="image">Image</label>
                    <input type="file"
                            value="{{ old('image') }}"
                            class="form-control @error('image') is-invalid @enderror"
                            name="image" id="image">
                    @error('image')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <button class="btn btn-success" type="submit">Add Post</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('page-level-scripts')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.3/trix.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

    <script>
        flatpickr("#published_at", {
            enableTime: true
        });
        $(document).ready(function(){
        });

        // Here this select2 code or function first written inside the document.ready but it fails, but when i put this method outside the ready function it works fine, i rlly dont know the reason why it behaves like this, but this is some kind of memory or can say mistake done from me, and its not wrong to place this select2 inside the ready, everybody's code was running fine inside the ready, but for mine , i dont know (english ke liye itna likha ;) )
        $('.select2').select2({
            placeholder : 'Select..'
        });
    </script>
@endsection

@section('page-level-styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.3/trix.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

@endsection
