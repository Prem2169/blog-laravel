@extends('layouts.app')

@section('content')

    <div class="card">


        <div class="card-header">Posts</div>

        <div class="card-body">
            <table class="table table-bordered">
                <thead>
                    <th>Image</th>
                    <th>Title</th>
                    <th>Excerpt</th>
                    <th>Category</th>
                    <th>Tags</th>
                    <th>Actions</th>
                </thead>
                <tbody>
                    @foreach ($posts as $post)
                        <tr>
                            <td><img src="{{ asset('storage/'.$post->image) }}" alt="" width="128px"></td>
                            <td>{{ $post->title }}</td>
                            <td>{{ $post->excerpt }}</td>
                            <td>{{ $post->category->name }}</td>
                            <td>
                                @foreach ($post->tags as $tag)
                                    {{ $tag->name }}
                                @endforeach
                            </td>
                            <td>
                                <a href=""
                                    class="btn btn-outline-primary btn-sm"
                                    onclick="displayRestoreModalForm({{ $post }})"
                                    data-toggle="modal"
                                    data-target="#restoreModal">Restore</a>
                                <a href=""
                                    class="btn btn-outline-danger btn-sm"
                                    onclick="displayModalForm({{ $post }})"
                                    data-toggle="modal"
                                    data-target="#deleteModal">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            {{ $posts->links() }}
        </div>
    </div>


    <!-- RESTORE Modal -->
    <div class="modal fade" id="restoreModal" tabindex="-1" role="dialog" aria-labelledby="restoreModal" aria-hidden="true">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="restoreModal">Restore Post</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <form action="" method="POST" id="restoreForm">
                @csrf
                @method('PUT')
                <div class="modal-body">
                    Are your sure you want to restore this post Permenantely
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Restore Post</button>
                </div>
            </form>
        </div>
        </div>
    </div>
    <!-- END Restore Modal-->

    <!-- DELETE Modal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="deleteModal">Delete Post</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <form action="" method="POST" id="deleteForm">
                @csrf
                @method('DELETE')
                <div class="modal-body">
                    Are your sure you want to delete this post Permenantely
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Delete Post Permenantely</button>
                </div>
            </form>
        </div>
        </div>
    </div>
    <!-- END Delete Modal-->

@endsection
@section('page-level-scripts')
    <script type="text/javascript">
        function displayModalForm($post)
        {
            var url = '/posts/' + $post.id;
            $('#deleteForm').attr('action', url);
        }
        function displayRestoreModalForm($post)
        {
            var url = '/restore/' + $post.id;
            $('#restoreForm').attr('action', url);
        }
    </script>
@endsection
