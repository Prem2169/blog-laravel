@extends('layouts.blog.blog_layout')

@section('title', 'Readit | Heaven for Bloggers')

{{-- @section('header')
    <header class="pt100 pb100 parallax-window-2" data-parallax="scroll" data-speed="0.5" data-image-src="assets/img/bg/img-bg-17.jpg" data-positiony="1000">
        <div class="intro-body text-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 pt50">
                        <h1 class="brand-heading font-montserrat text-uppercase color-light" data-in-effect="fadeInDown">
                            Pen-It
                            <small class="color-light alpha7">Heaven for Bloggers!</small>
                        </h1>
                    </div>
                </div>
            </div>

        </div>
    </header>

@endsection --}}

@section('header')
    <div class="hero-wrap js-fullheight" style="background-image: url('{{ asset('images/bg_1.jpg') }}');" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
        <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-start" data-scrollax-parent="true">
            <div class="col-md-12">
                <h2 class="subheading">Hello! Welcome to</h2>
                <h1 class="mb-4 mb-md-0">Readit blog</h1>
                <div class="row">
                    <div class="col-md-7">
                        <div class="text">
                            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                            <div class="mouse">
                                <a href="#" class="mouse-icon">
                                    <div class="mouse-wheel"><span class="ion-ios-arrow-round-down"></span></div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
@endsection

@section('main-content')
    <div class="container">

    {{-- Posts --}}
    <div class="row d-flex">

        {{-- POSTS --}}
        @foreach ($posts as $post)
            <?php
                if($post->published_at == null){
                    $date = '0';
                    $month = '0';
                    $year = '0';
                }
                else{
                    $dateTime = new DateTime($post->published_at);
                    $date = $dateTime->format('d');
                    $month = $dateTime->format('M');
                    $year = $dateTime->format('Y');
                }
            ?>
            <div class="col-md-4 d-flex ">
                <div class="blog-entry justify-content-end">
                    <a href="blog-single.html" class="block-20" style="background-image: url('{{ asset('storage/'.$post->image) }}'); height: 40vh;">
                    </a>
                    <div class="text p-4 float-right d-block">
                        <div class="topper d-flex align-items-center">
                            <div class="one py-2 pl-3 pr-1 align-self-stretch">
                                <span class="day">{{ $date }}</span>
                            </div>
                            <div class="two pl-0 pr-0 py-2 align-self-stretch">
                                <span class="yr pr-0">{{ $year }}</span>
                                <span class="mos pr-0">{{ $month }}</span>
                            </div>
                            <span class="day" style="font-size: 1.8rem;"><i class="icon-person pr-1"></i></span>
                            <div class="three pl-0 pr-3 py-2 align-self-stretch">
                                {{-- <span class="icon-pencil"></span> --}}
                                <span class="mos">{{ $post->author->name }}</span>
                            </div>
                        </div>
                        <h3 class="heading mb-3" style="height: 7rem"><a href="#">{{ $post->title }}</a></h3>
                        <p style="height: 8rem">{{ $post->excerpt }}</p>
                        <p class=""><a href="{{ route('blog.show', $post->id) }}" class="btn-custom"><span class="ion-ios-arrow-round-forward mr-3"></span>Read more</a></p>
                    </div>
                </div>
            </div>

        @endforeach
        {{-- POSTS END --}}

    </div>
    {{-- / Posts --}}

    {{-- PAGINATION --}}

    {{ $posts->appends(['search'=>request('search')])->links() }}

    {{-- Here i have changed the views of the pagination as we want the links in our styles, so the steps are written in docs--}}
    {{-- written the code of this links inside the 'views/vendor/pagination/bootstrap-4.php' iske andar likha h --}}
    {{-- PAGINATION END --}}

  </div>
@endsection
