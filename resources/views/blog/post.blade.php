@extends('layouts.blog.blog_layout')

@section('title', 'Single Post')

@section('header')
    <section class="hero-wrap hero-wrap-2 js-fullheight" style="background-image: url('{{ asset('images/bg_1.jpg') }}');" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
        <div class="row no-gutters slider-text js-fullheight align-items-end justify-content-center">
            <div class="col-md-9 pb-5 text-center">
            <h1 class="mb-3 bread">Blog Single</h1>
            <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home <i class="ion-ios-arrow-forward"></i></a></span> <span class="mr-2"><a href="blog.html">Blog <i class="ion-ios-arrow-forward"></i></a></span> <span>Blog Single <i class="ion-ios-arrow-forward"></i></span></p>
            </div>
        </div>
        </div>
    </section>
@endsection
@section('main-content')

    <div>
        <p class="mb-5">
        <img src="{{ asset('storage/' . $post->image) }}" alt="" class="img-fluid">
        </p>

        <div class="blog-three-attrib d-flex mb-50">
            <div class="ml-1 mr-1"><i class="fa fa-calendar"></i>{{ $post->published_at->diffForHumans() }}</div> |
            <div class="ml-1 mr-1"><i class="fa fa-comment-o"></i><a href="#">90 Comments</a></div> |
            <div class="ml-1 mr-1"><a href="#"><i class="fa fa-thumbs-o-up"></i></a>150 Likes</div>
        </div>

        <h2 class="mb-3">{{ $post->title }}</h2>

        <p class="mb-50" style="font-weight: 800">{{ $post->excerpt }}</p>
        <p>{!! $post->content !!}</p>
        <div class="tag-widget post-tag-container mb-5 mt-5">
            <h3>Tags</h3>
            <div class="tagcloud">
                @foreach ($post->tags as $tag)
                    <a href="#" class="tag-cloud-link">{{ $tag->name }}</a>
                @endforeach
            </div>
        </div>

        <div class="about-author d-flex p-4 bg-light">
            <div class="bio mr-5">
                <img src="{{ \Thomaswelton\LaravelGravatar\Facades\Gravatar::src($post->author->email) }}" alt="Image placeholder" class="img-fluid mb-4">
            </div>
            <div class="desc">
            <h3>{{ $post->author->name }}</h3>
            <p>{{ $post->author->about }}</p>
            </div>
        </div>
        {{-- Comments --}}



        <div id="disqus_thread"></div>
        <script>

            /**
            *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
            *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/

            var disqus_config = function () {
            this.page.url = "{{ route('blog.show', $post->id) }}";  // Replace PAGE_URL with your page's canonical URL variable
            this.page.identifier = {{ $post->id }}; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
            };

            (function() { // DON'T EDIT BELOW THIS LINE
                var d = document, s = d.createElement('script');
                s.src = 'https://laravel-blog-m3ojcagrl1.disqus.com/embed.js';
                s.setAttribute('data-timestamp', +new Date());
                console.log('TP');
                (d.head || d.body).appendChild(s);
            })();
        </script>
        <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>


        {{-- / Comments --}}

    </div>

@endsection
