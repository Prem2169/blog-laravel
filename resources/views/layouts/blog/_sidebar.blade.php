<div class="col-lg-4 sidebar pl-lg-5">

    {{-- SEARCH BAR --}}
    <div class="sidebar-box">
      <form action="" class="search-form">
        <div class="form-group">
          <span class="icon icon-search"></span>
          <input type="text" name="search" class="form-control" placeholder="Type a keyword and hit enter" value="{{ request('search') }}">
        </div>
      </form>
    </div>
    {{-- SEARCH BAR END--}}

    {{-- CATEGORIES --}}
    <div class="sidebar-box">
      <div class="categories">
        <h3>Categories</h3>
        @foreach ($categories as $category)
            <li><a href="{{ route('blog.category', $category->id) }}">{{ $category->name }}<span class="ion-ios-arrow-forward"></span></a></li>
        @endforeach
      </div>
    </div>
    {{-- CATEGORIES END--}}

    {{-- TAGS --}}
    <div class="sidebar-box">
        <h3>Tag Cloud</h3>
        <div class="tagcloud">
            @foreach ($tags as $tag)
              <a href="{{ route('blog.tag', $tag->id) }}" class="tag-cloud-link">{{ $tag->name }}</a>
            @endforeach
        </div>
    </div>
    {{-- TAGS END--}}

    {{-- RECENT POSTS --}}
    <div class="sidebar-box">
      <h3>Recent Blog</h3>
      <div class="block-21 mb-4 d-flex">
        <a class="blog-img mr-4" style="background-image: url({{ asset('images/image_1.jpg') }});"></a>
        <div class="text">
          <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the blind texts</a></h3>
          <div class="meta">
            <div><a href="#"><span class="icon-calendar"></span> Nov. 14, 2019</a></div>
            <div><a href="#"><span class="icon-person"></span> Admin</a></div>
            <div><a href="#"><span class="icon-chat"></span> 19</a></div>
          </div>
        </div>
      </div>
      <div class="block-21 mb-4 d-flex">
        <a class="blog-img mr-4" style="background-image: url({{ asset('images/image_2.jpg') }});"></a>
        <div class="text">
          <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the blind texts</a></h3>
          <div class="meta">
            <div><a href="#"><span class="icon-calendar"></span> Nov. 14, 2019</a></div>
            <div><a href="#"><span class="icon-person"></span> Admin</a></div>
            <div><a href="#"><span class="icon-chat"></span> 19</a></div>
          </div>
        </div>
      </div>
      <div class="block-21 mb-4 d-flex">
        <a class="blog-img mr-4" style="background-image: url({{ asset('images/image_3.jpg') }});"></a>
        <div class="text">
          <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the blind texts</a></h3>
          <div class="meta">
            <div><a href="#"><span class="icon-calendar"></span> Nov. 14, 2019</a></div>
            <div><a href="#"><span class="icon-person"></span> Admin</a></div>
            <div><a href="#"><span class="icon-chat"></span> 19</a></div>
          </div>
        </div>
      </div>
    </div>
    {{-- RECENT POSTS END --}}

    <div class="sidebar-box">
      <h3>Paragraph</h3>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus itaque, autem necessitatibus voluptate quod mollitia delectus aut, sunt placeat nam vero culpa sapiente consectetur similique, inventore eos fugit cupiditate numquam!</p>
    </div>
  </div>
