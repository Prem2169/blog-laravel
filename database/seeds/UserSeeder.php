<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where('email', 'prem.lalchandani.46@gmail.com')->get()->first();
        if(!$user)
        {
            User::create([
                'name' => 'Prem Lalchandani',
                'email' => 'prem.lalchandani.46@gmail.com',
                'password'=> Hash::make('abcd1234'),
                'role'=> 'admin',
            ]);
        }
        else
        {
            $user->update(['role' => 'admin']);
        }

        User::create([
            'name' => 'Niraj Bathija',
            'email' => 'niraj@gmail.com',
            'password'=> Hash::make('abcd1234'),
        ]);

        User::create([
            'name' => 'Rohit Udasi',
            'email' => 'rohit@gmail.com',
            'password'=> Hash::make('abcd1234'),
        ]);


    }
}
