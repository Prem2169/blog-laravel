<?php

use App\Category;
use App\Post;
use App\Tag;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categoryNews = Category::create(['name' => 'News']);
        $categoryDesign = Category::create(['name' => 'Design']);
        $categoryTechnology = Category::create(['name' => 'Technology']);
        $categoryEngineering = Category::create(['name' => 'Engineering']);

        $tagCustomers = Tag::create(['name'=>'customers']);
        $tagLaravel = Tag::create(['name'=>'laravel']);
        $tagCoding = Tag::create(['name'=>'coding']);
        $tagDesign = Tag::create(['name'=>'design']);

        $post1 = Post::create([
            'title' => 'We Relocated office to HOME post1',
            'excerpt'=>Faker\Factory::create()->sentence(10),
            'content'=>Faker\Factory::create()->paragraphs(rand(3,7), true),
            'image'=>'posts/1.jpg',
            'category_id'=>$categoryDesign->id,
            'user_id'=>2,
            'published_at'=>Carbon::now()->format('Y-m-d')
        ]);
        $post2 = Post::create([
            'title' => 'We Relocated office to HOME post2',
            'excerpt'=>Faker\Factory::create()->sentence(10),
            'content'=>Faker\Factory::create()->paragraphs(rand(3,7), true),
            'image'=>'posts/2.jpg',
            'category_id'=>$categoryNews->id,
            'user_id'=>3,
            'published_at'=>Carbon::now()->format('Y-m-d')
        ]);
        $post3 = Post::create([
            'title' => 'We Relocated office to HOME post3',
            'excerpt'=>Faker\Factory::create()->sentence(10),
            'content'=>Faker\Factory::create()->paragraphs(rand(3,7), true),
            'image'=>'posts/3.jpg',
            'category_id'=>$categoryDesign->id,
            'user_id'=>2,
            'published_at'=>Carbon::now()->format('Y-m-d')
        ]);
        $post4 = Post::create([
            'title' => 'We Relocated office to HOME post4',
            'excerpt'=>Faker\Factory::create()->sentence(10),
            'content'=>Faker\Factory::create()->paragraphs(rand(3,7), true),
            'image'=>'posts/4.jpg',
            'category_id'=>$categoryTechnology->id,
            'user_id'=>2,
            'published_at'=>Carbon::now()->format('Y-m-d')
        ]);
        $post5 = Post::create([
            'title' => 'We Relocated office to HOME post5',
            'excerpt'=>Faker\Factory::create()->sentence(10),
            'content'=>Faker\Factory::create()->paragraphs(rand(3,7), true),
            'image'=>'posts/5.jpg',
            'category_id'=>$categoryEngineering->id,
            'user_id'=>3,
            'published_at'=>Carbon::now()->format('Y-m-d')
        ]);

        $post1->tags()->attach([$tagCoding->id, $tagCustomers->id]);
        $post2->tags()->attach([$tagDesign->id, $tagLaravel->id]);
        $post3->tags()->attach([$tagCoding->id, $tagLaravel->id]);
        $post4->tags()->attach([$tagDesign->id, $tagLaravel->id, $tagCustomers->id]);
        $post5->tags()->attach([$tagCoding->id, $tagDesign->id]);
    }
}
